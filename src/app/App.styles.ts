import styled, { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
  body {
    font-family: 'Poppins', sans-serif;
    box-sizing: border-box;
    padding: 0px;
    margin: 0px;
  }
`;

export const Container = styled.div`
  background: #f4f4f4;
  min-height: 100vh;
`;

export const Main = styled.main`
  max-width: 933px;
  margin: 0 auto;

  @media (max-width: 768px) {
    padding: 0px 20px;
  }
`;

export const Brand = styled.img`
  margin: 30px 0px 50px 0px;
`;
