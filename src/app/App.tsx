import React from "react";
import { Home } from "screens/home/Home";

import * as Styles from "./App.styles";

export function App() {
  return (
    <React.Fragment>
      <Styles.Container>
        <Styles.Main>
          <Styles.Brand src="https://www.webmotors.com.br/assets/img/webmotors.svg?t=1.0.0" />
          <Home />
        </Styles.Main>
      </Styles.Container>
      <Styles.GlobalStyle />
    </React.Fragment>
  );
}
