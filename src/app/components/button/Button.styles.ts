import styled from "styled-components";

export const Button = styled.button`
  background: transparent;
  transition: 0.25s;
  height: 50px;
  width: 100%;

  &:hover {
    cursor: pointer;
  }

  &:active {
    transform: scale(0.95);
  }

  &.theme-primary {
    text-transform: uppercase;
    background: #ca2431;
    font-weight: bold;
    font-size: 16px;
    max-width: 100%;
    border: 0px;
    color: #fff;
  }

  &.theme-warning {
    border: 2px solid #c9a669;
    font-weight: bold;
    max-width: 200px;
    color: #c9a669;
    height: 40px;

    @media (max-width: 768px) {
      max-width: 100%;
    }
  }

  &.theme-danger {
    border: 0px;
    color: #ca2431;
    font-weight: bold;
    width: auto;
  }

  &.theme-light {
    font-weight: bold;
    border: 0px;
    color: #999;
  }
`;
