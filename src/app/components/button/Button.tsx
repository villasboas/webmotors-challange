import * as Styles from "./Button.styles";

export type ButtonProps = {
  label: string;
  theme?: "primary" | "warning" | "danger" | "light";
};

export function Button(props: ButtonProps) {
  const { label, theme = "light" } = props;

  return (
    <Styles.Button className={[`theme-${theme}`].join(" ")}>
      {label}
    </Styles.Button>
  );
}
