import styled from "styled-components";

export const Container = styled.label`
  align-items: center;
  margin-right: 15px;
  font-size: 12px;
  display: flex;
`;

export const Input = styled.input`
  margin-right: 8px;
  height: 18px;
  width: 18px;
`;
