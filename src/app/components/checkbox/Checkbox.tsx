import * as Styles from "./Checkbox.styles";

export type CheckboxProps = {
  label: string;
};

export function Checkbox(props: CheckboxProps) {
  const { label } = props;

  return (
    <Styles.Container>
      <Styles.Input type="checkbox" id="checkbox" />
      <div>{label}</div>
    </Styles.Container>
  );
}
