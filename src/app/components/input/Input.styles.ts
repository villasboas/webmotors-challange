import styled from "styled-components";

export const Container = styled.label`
  border: 1px solid #ccc;
  border-right: 0px;
  padding: 0px 10px;
  background: #fff;
  display: flex;
  height: 40px;
`;

export const Input = styled.input`
  padding: 0px 10px;
  outline: none;
  width: 100%;
  border: 0px;
`;

export const Label = styled.div`
  align-items: center;
  font-size: 12px;
  display: flex;
`;
