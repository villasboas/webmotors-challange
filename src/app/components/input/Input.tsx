import * as Styles from "./Input.styles";

export type InputProps = {
  label: string;
  type: string;
};

export function Input(props: InputProps) {
  return (
    <Styles.Container>
      <Styles.Label>{props.label}</Styles.Label>
      <Styles.Input type={props.type} />
    </Styles.Container>
  );
}
