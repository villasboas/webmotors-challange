import styled from "styled-components";

export const Container = styled.label`
  border: 1px solid #ccc;
  padding: 0px 10px;
  background: #fff;
  display: flex;
  height: 40px;
`;

export const Label = styled.div`
  white-space: nowrap;
  align-items: center;
  font-size: 12px;
  display: flex;
`;

export const Select = styled.select`
  background: transparent;
  outline: none;
  border: none;
  width: 100%;
`;
