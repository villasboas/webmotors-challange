import { Controller } from "react-hook-form";
import * as Styles from "./Select.styles";

export type Options = {
  value: string | number;
  label: string;
};

export type SelectProps = {
  loading?: boolean;
  control?: any;
  name: string;
  options: Options[];
  label: string;
};

export function Select(props: SelectProps) {
  const { label, name, control } = props;

  return (
    <Controller
      name={name}
      control={control}
      render={({ field }) => (
        <Styles.Container>
          <Styles.Label>{label}</Styles.Label>
          <Styles.Select {...field} name="name">
            {props.options.map((option) => (
              <option key={option.value} value={option.value}>
                {option.label}
              </option>
            ))}
          </Styles.Select>
        </Styles.Container>
      )}
    />
  );
}
