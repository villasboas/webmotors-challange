import styled from "styled-components";

export const Container = styled.div`
  border-bottom: 3px solid transparent;
  padding: 15px 25px;
  transition: 0.25s;
  display: flex;
  color: #999;

  &:hover {
    cursor: pointer;
    color: #333;
  }

  &.actived {
    border-color: #ca2431;
    color: #ca2431;
  }
`;

export const Icon = styled.div`
  align-items: flex-end;
  padding: 0px 15px;
  font-size: 24px;
  display: flex;
`;

export const Label = styled.div`
  text-transform: uppercase;
  line-height: 12px;
  font-size: 12px;
  color: #999;
`;

export const Title = styled.div`
  text-transform: uppercase;
  line-height: 25px;
  font-size: 22px;
`;
