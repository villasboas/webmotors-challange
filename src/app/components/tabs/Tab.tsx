import { useContext, useMemo } from "react";

import { TabsContext } from "./TabContext";
import * as Styles from "./Tab.styles";

export type TabProps = {
  label: string;
  title: string;
  icon: any;
};

export function Tab(props: TabProps) {
  const { setSelected, selected } = useContext(TabsContext);

  const actived = useMemo(() => {
    return selected === props.title;
  }, [selected, props.title]);

  return (
    <Styles.Container
      className={`${actived && "actived"}`}
      onClick={() => setSelected(props.title)}
    >
      <Styles.Icon>{props.icon}</Styles.Icon>
      <div>
        <Styles.Label>{props.label}</Styles.Label>
        <Styles.Title>{props.title}</Styles.Title>
      </div>
    </Styles.Container>
  );
}
