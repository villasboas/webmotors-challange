import React from "react";

export type TabContextProps = {
  setSelected: any;
  selected: string;
};

export const TabsContext = React.createContext<TabContextProps>({
  setSelected: null,
  selected: "",
});
