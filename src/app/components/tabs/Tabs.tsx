import { useState } from "react";
import { TabsContext } from "./TabContext";

import * as Styles from "./Tabs.styles";

export type TabsProps = {
  children: React.ReactNode;
  value: string;
};

export function Tabs(props: TabsProps) {
  const [selected, setSelected] = useState<string>(props.value);
  const { children } = props;

  return (
    <TabsContext.Provider value={{ selected, setSelected }}>
      <Styles.Container>{children}</Styles.Container>
    </TabsContext.Provider>
  );
}
