import { useEffect, useMemo, useState } from "react";
import { makeService } from "..";
import { Make } from "../models/Make";

export function useMakes() {
  const [loading, setLoading] = useState<boolean>(false);
  const [makes, setMakes] = useState<Make[]>([]);

  const makesOptions = useMemo(
    function () {
      const makesArr = makes.map((make) => ({
        value: make.id,
        label: make.name,
      }));

      return [{ value: "", label: "Todas" }, ...makesArr];
    },
    [makes]
  );

  useEffect(function () {
    setLoading(true);
    makeService
      .getMakes()
      .then(function (makes) {
        setMakes(makes);
        setLoading(false);
      })
      .catch(function () {
        setLoading(false);
        setMakes([]);
      });
  }, []);

  return { makesOptions, makes, loading };
}
