export class Make {
  id!: number;
  name!: string;

  constructor(data: Partial<Make> = {}) {
    Object.assign(this, data);
  }
}
