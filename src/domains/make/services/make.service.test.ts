import { apiService } from "app/services/api.service";
import { Make } from "..";
import * as sut from "./make.service";

jest.mock("app/services/api.service");

const VALID_MAKE = {
  ID: "<valid_id>",
  Name: "<valid_name>",
};

describe("Make Service", () => {
  it("should list all available makes", async () => {
    (apiService as any).get.mockResolvedValue({
      data: [VALID_MAKE],
    });

    const result = await sut.getMakes();

    expect((apiService as any).get).toHaveBeenCalledWith("/Make");
    expect(result.length).toBe(1);
    expect(result?.[0]).toBeInstanceOf(Make);
    expect(result?.[0].id).toBe(VALID_MAKE.ID);
    expect(result?.[0].name).toBe(VALID_MAKE.Name);
  });

  it("should return empty array if list is empty", async () => {
    (apiService as any).get.mockResolvedValue({
      data: [],
    });

    const result = await sut.getMakes();

    expect((apiService as any).get).toHaveBeenCalledWith("/Make");
    expect(result.length).toBe(0);
    expect(result?.[0]).toBeUndefined();
  });
});
