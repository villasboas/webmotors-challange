import { apiService } from "app/services/api.service";
import { Make } from "..";

export function getMakes(): Promise<Make[]> {
  return apiService.get("/Make").then(({ data }) =>
    data.map(
      (make: any) =>
        new Make({
          id: make.ID,
          name: make.Name,
        })
    )
  );
}
