import { useEffect, useMemo, useState } from "react";
import { modelService } from "..";
import { Model } from "../models/Model";

export function useModels(makeId: string) {
  const [models, setModels] = useState<Model[]>([]);
  const [loading, setLoading] = useState(false);

  const modelsOptions = useMemo(
    function () {
      const modelsArr = models.map((make) => ({
        value: make.id,
        label: make.name,
      }));

      return [{ value: "", label: "Todas" }, ...modelsArr];
    },
    [models]
  );

  useEffect(
    function () {
      if (!makeId) return;

      setLoading(true);
      modelService
        .getModelsByMake(Number.parseInt(makeId))
        .then(function (models) {
          setModels(models);
          setLoading(false);
        })
        .catch(function () {
          setLoading(false);
          setModels([]);
        });
    },
    [makeId]
  );

  return { modelsOptions, models, loading };
}
