export class Model {
  id!: number;
  name!: string;
  makeId!: number;

  constructor(data: Partial<Model> = {}) {
    Object.assign(this, data);
  }
}
