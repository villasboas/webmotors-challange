import { apiService } from "app/services/api.service";
import { Model } from "..";
import * as sut from "./model.service";

jest.mock("app/services/api.service");

const VALID_MAKE_ID = 1;

const VALID_MODEL = {
  ID: "<valid_id>",
  Name: "<valid_name>",
  MakeID: VALID_MAKE_ID,
};

describe("Make Service", () => {
  it("should list all available models", async () => {
    (apiService as any).get.mockResolvedValue({
      data: [VALID_MODEL],
    });

    const result = await sut.getModelsByMake(VALID_MAKE_ID);

    expect((apiService as any).get).toHaveBeenCalledWith("/Model", {
      params: {
        MakeId: VALID_MAKE_ID,
      },
    });
    expect(result.length).toBe(1);
    expect(result?.[0]).toBeInstanceOf(Model);
    expect(result?.[0].id).toBe(VALID_MODEL.ID);
    expect(result?.[0].name).toBe(VALID_MODEL.Name);
    expect(result?.[0].makeId).toBe(VALID_MAKE_ID);
  });

  it("should return empty array if list is empty", async () => {
    (apiService as any).get.mockResolvedValue({
      data: [],
    });

    const result = await sut.getModelsByMake(VALID_MAKE_ID);

    expect((apiService as any).get).toHaveBeenCalledWith("/Model", {
      params: {
        MakeId: VALID_MAKE_ID,
      },
    });
    expect(result.length).toBe(0);
    expect(result?.[0]).toBeUndefined();
  });
});
