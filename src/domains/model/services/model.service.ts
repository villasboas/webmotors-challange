import { apiService } from "app/services/api.service";
import { Model } from "..";

export function getModelsByMake(makeId: number): Promise<Model[]> {
  return apiService
    .get("/Model", {
      params: {
        MakeId: makeId,
      },
    })
    .then(({ data }) =>
      data.map(
        (make: any) =>
          new Model({
            id: make.ID,
            name: make.Name,
            makeId: make.MakeID,
          })
      )
    );
}
