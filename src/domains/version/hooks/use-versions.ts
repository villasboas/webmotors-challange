import { useEffect, useMemo, useState } from "react";
import { Version } from "../models/Version";
import { versionService } from "..";

export function useVersions(modelId: string) {
  const [versions, setVersions] = useState<Version[]>([]);
  const [loading, setLoading] = useState(false);

  const versionsOptions = useMemo(
    function () {
      const modelsArr = versions.map((version) => ({
        value: version.id,
        label: version.name,
      }));

      return [{ value: "", label: "Todas" }, ...modelsArr];
    },
    [versions]
  );

  useEffect(
    function () {
      if (!modelId) return;

      setLoading(true);
      versionService
        .getVersionsByModelId(Number.parseInt(modelId))
        .then(function (version) {
          setVersions(version);
          setLoading(false);
        })
        .catch(function () {
          setLoading(false);
          setVersions([]);
        });
    },
    [modelId]
  );

  return { versionsOptions, versions, loading };
}
