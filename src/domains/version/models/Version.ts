export class Version {
  id!: number;
  name!: string;
  modelId!: number;

  constructor(data: Partial<Version> = {}) {
    Object.assign(this, data);
  }
}
