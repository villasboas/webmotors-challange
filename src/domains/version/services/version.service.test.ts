import { apiService } from "app/services/api.service";
import { Version } from "..";
import * as sut from "./version.service";

jest.mock("app/services/api.service");

const VALID_MODEL_ID = 1;

const VALID_VERSION = {
  ID: "<valid_id>",
  Name: "<valid_name>",
  ModelID: VALID_MODEL_ID,
};

describe("Make Service", () => {
  it("should list all available models", async () => {
    (apiService as any).get.mockResolvedValue({
      data: [VALID_VERSION],
    });

    const result = await sut.getVersionsByModelId(VALID_MODEL_ID);

    expect((apiService as any).get).toHaveBeenCalledWith("/Version", {
      params: {
        ModelId: VALID_MODEL_ID,
      },
    });
    expect(result.length).toBe(1);
    expect(result?.[0]).toBeInstanceOf(Version);
    expect(result?.[0].id).toBe(VALID_VERSION.ID);
    expect(result?.[0].name).toBe(VALID_VERSION.Name);
    expect(result?.[0].modelId).toBe(VALID_MODEL_ID);
  });

  it("should return empty array if list is empty", async () => {
    (apiService as any).get.mockResolvedValue({
      data: [],
    });

    const result = await sut.getVersionsByModelId(VALID_MODEL_ID);

    expect((apiService as any).get).toHaveBeenCalledWith("/Version", {
      params: {
        ModelId: VALID_MODEL_ID,
      },
    });
    expect(result.length).toBe(0);
    expect(result?.[0]).toBeUndefined();
  });
});
