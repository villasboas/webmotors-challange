import { apiService } from "app/services/api.service";
import { Version } from "../models/Version";

export function getVersionsByModelId(modelId: number): Promise<Version[]> {
  return apiService
    .get("/Version", {
      params: {
        ModelId: modelId,
      },
    })
    .then(({ data }) =>
      data.map(
        (make: any) =>
          new Version({
            id: make.ID,
            name: make.Name,
            modelId: make.ModelID,
          })
      )
    );
}
