import styled from "styled-components";

export const Card = styled.form``;

export const CardHeader = styled.div`
  justify-content: space-between;
  display: flex;

  @media (max-width: 768px) {
    flex-direction: column-reverse;
  }
`;

export const CardBody = styled.div`
  justify-content: space-between;
  flex-direction: column;
  padding: 30px 60px;
  min-height: 212px;
  background: #fff;
  display: flex;
`;

export const Grid = styled.div<any>`
  grid-template-columns: ${({ columns }) => columns};
  gap: ${({ gap }) => gap || "15px"};
  display: grid;

  @media (max-width: 768px) {
    display: ${({ noFlex }) => (noFlex ? "grid" : "flex")};
    flex-direction: column;

    &.mobile-mt-20 {
      margin-top: 20px;
    }
  }
`;

export const Flex = styled.div<any>`
  justify-content: ${({ justifyContent }) => justifyContent};
  display: flex;
`;
