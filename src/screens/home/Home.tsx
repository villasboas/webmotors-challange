import { AiOutlineCar } from "react-icons/ai";
import { FaMotorcycle } from "react-icons/fa";

import { Checkbox, Button, Tab, Tabs, Input, Select } from "app/components";
import * as Styles from "./Home.styles";
import { useForm } from "react-hook-form";
import { useMakes } from "domains/make";
import { useModels } from "domains/model";
import { useVersions } from "domains/version";

export function Home() {
  const { control, watch } = useForm({
    defaultValues: {
      type: "car",
      make: "",
      model: "",
      year: "",
      location: "",
      radius: "",
    },
  });
  const { versionsOptions } = useVersions(watch("model"));
  const { modelsOptions } = useModels(watch("make"));
  const { makesOptions } = useMakes();

  return (
    <Styles.Card>
      <Styles.CardHeader>
        <Tabs value="carro">
          <Tab label="comprar" title="carro" icon={<AiOutlineCar />} />
          <Tab label="comprar" title="moto" icon={<FaMotorcycle />} />
        </Tabs>
        <Button label="Vender meu carro" theme="warning" />
      </Styles.CardHeader>
      <Styles.CardBody>
        <Styles.Flex>
          <Checkbox label="Novos" />
          <Checkbox label="Usados" />
        </Styles.Flex>
        <Styles.Grid className="mobile-mt-20" columns="1fr 1fr">
          <Styles.Grid noFlex columns="2fr 1fr" gap="0px">
            <Input label="Onde" type="search" />
            <Select name="radius" control={control} label="Raio" options={[]} />
          </Styles.Grid>
          <Styles.Grid columns="1fr 1fr">
            <Select
              name="make"
              control={control}
              label="Marca"
              options={makesOptions}
            />
            <Select
              name="model"
              control={control}
              label="Modelo"
              options={modelsOptions}
            />
          </Styles.Grid>
        </Styles.Grid>
        <Styles.Grid className="mobile-mt-20" columns="1fr 1fr">
          <Styles.Grid columns="1fr 1fr">
            <Select
              name="year"
              control={control}
              label="Ano Desejado"
              options={[]}
            />
            <Select
              name="price"
              control={control}
              label="Faixa de Preço"
              options={[]}
            />
          </Styles.Grid>
          <Styles.Grid columns="1fr">
            <Select
              name="version"
              control={control}
              label="Versão"
              options={versionsOptions}
            />
          </Styles.Grid>
        </Styles.Grid>
        <Styles.Grid columns="1fr 1fr">
          <div>
            <Button label="Busca Avançada" theme="danger" />
          </div>
          <Styles.Grid columns="1fr 2.5fr">
            <Button label="Limpar filtros" />
            <Button label="Ver ofertas" theme="primary" />
          </Styles.Grid>
        </Styles.Grid>
      </Styles.CardBody>
    </Styles.Card>
  );
}
